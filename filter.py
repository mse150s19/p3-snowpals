import skimage
import cv2
import numpy as np
from skimage import data
from skimage import color
from skimage import io
from PIL import Image
from scipy.misc import imread, imsave

image = color.rgb2gray(io.imread('pictures/logspiral.png'))
image = image * 255
threshold = 200 
binarized = 1.0 *(image < threshold)
binarized = binarized * 255
cv2.imshow('black_and_white_image', binarized)
cv2.waitKey(0)
cv2.imwrite('pictures/filtered_logspiral.png', binarized)

