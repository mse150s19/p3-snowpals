import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
from scipy.spatial import cKDTree
from skimage import io
from skimage import color
from skimage.filters import threshold_otsu
from skimage.morphology import skeletonize
from skimage import data
from scipy import ndimage
from skimage.filters import sobel
import random

rad=1.5
bigNum=20000	#int for max iterations.
stepSze = 0.11

def main():

    # Make a binary image to work with.
    image = color.rgb2gray(io.imread('filtered_logspiral.png'))

    #filtrdImg = filterImage(image)
    #plotter1(image,filtrdImg,'filtered')
    image = image/255
    #img = np.invert(image)
  
    # perform skeletonization
    skeleton = skeletonize(image)
    plotter1(image,np.invert(skeleton),'skeleton')
    #exit()
    
    #print(skeleton.shape)
    rows, cols = np.where(skeleton)
    skelCoords = np.array([rows,cols]).T
    #print(skelCoords.shape[0])
    #print(skelCoords)

    # Set origin and end. Detecting these would be awesomer but that's hard.
    #orig = np.array([787,248])
    orig = np.array([334,280])
    terminus = np.array([335,518])

    #plotter1(image,np.invert(skeleton),'skeleton')

    #get the main backbone of coil, removing extra pixels.
    skel_xy = trim_skel(skelCoords,orig,terminus)
    #print(skel_xy[2250:])


    #skel = np.unique(skel_xy,axis=0)
    #print(skel.shape[0])

    doSciencePleaseThanks(skel_xy,orig)

    plotter(image,orig,skel_xy,skeleton)

def filterImage(image):
 
    #binary hamhand.
    thresh = threshold_otsu(image)
    thresh = 90 
    binarized = (image > thresh)

    #img = image.copy()
    #thresh = 0.9991
    #mask1 = img > thresh
    #img[mask1] = 1

    #mask2 = img < thresh 
    #img[mask2] = 0

    #otsu filtering.
    #thresh = filters.threshold_otsu(image)
    #binary = image > thresh

    #block_size = 11
    #local_thresh = filters.threshold_local(image, block_size, offset=0)
    #binary_local = image > local_thresh
    #binary = np.invert(binary_local)


    #thresh = img_eq.mean()
    #thresh = filters.threshold_local(img_eq, block_size, offset=10)
    #thresh = filters.threshold_otsu(img_eq)
    #binary = img_eq > thresh

    #radius = 15
    #selem = disk(radius)
    #local_otsu = rank.otsu(image, selem)
    #img = image >= local_otsu

    #return img
    return binarized



def trim_skel(skelCoords,orig,terminus):

    #Put the skeleton coordinates on a tree.
    skelTree = cKDTree(skelCoords)

    #Get the orig/term indices.
    d0,indxOrig = skelTree.query(orig,k=1)
    dT,indxTerm = skelTree.query(terminus,k=1)
    #And their coords.
    orig_xy = np.array(skelCoords[indxOrig])
    term_xy = np.array(skelCoords[indxTerm])
    #print("ORIGIN: ",orig_xy, indxOrig)
    #print("TERM: ",term_xy, indxTerm)
    #print("\n")

    #iteratively trim the tree.
    done = False
    while( not done):

        #Create tree, again, and with every iteration.
        skelTree = cKDTree(skelCoords)

        #find the leaves/juncs/ends of the tree.
        loners,juncs,ends = find_leafs(skelTree,skelCoords)
        #print("loners: ", loners)
        #print("juncs: ", juncs)
        #print("ends: ", ends)
 
        #Make an index list of the unique ends.
        uniqEnds = np.unique(ends).tolist()
        #print(uniqEnds)
        #endCoords = np.array(skelCoords[uniqEnds])
        #print(endCoords)

        #add the loners - extra pixels - to the list for removal.
        uniqEnds.extend(loners.flatten().tolist())
        remList = uniqEnds.copy()
        #print(remList)

        #endCoords consists of just those points to remove.
        #endCoords = np.array(skelCoords[remList])

	#delete all ends remaining from the coordinate list, including origin
        #and terminus.  
        sk2=np.delete(skelCoords,remList,0)

        #add the origin and terminus coordinates back. Not their indices.
        sk2 = np.vstack((sk2,orig_xy,term_xy))
    
        skelCoords=np.copy(sk2)

        #when no trimming is left to be performed, exit.
        if len(remList) == 2:   
        #if len(remList) == 0:   
            done = True
 
    #Create tree, last time.
    #skelTree = cKDTree(skelCoords)

    #find the leaves/juncs/ends of the tree, last time.
    #loners,juncs,ends = find_leafs(skelTree,skelCoords)
    #junk = np.unique(juncs).tolist()
    #print(len(junk))
    #sk2=np.delete(skelCoords,junk,0) #remove the junctions.
    #skelCoords=np.copy(sk2)

    #make unique - might not be necessary.
    skel_coords = np.unique(skelCoords,axis=0)

    #order the coordinates from origin out.
    skelcoords = order_coords(skel_coords,orig_xy,term_xy)

    #print(skel_coords)

    #and return skelcoords
    #return skel_coords
    return skelcoords


def find_leafs(T,skelCoords):

    #First, find all ends where there is only one nearest neighbor, or fewer.
    endBranches = []
    isolatd = []
    juncts = []
    numCoords = skelCoords.shape[0]

    for i in range(0,numCoords):
        target = skelCoords[i]
        ii = T.query_ball_point(target,r=rad)

        if len(ii) == 1: 
     #       print("LONER: ", i, skelCoords[i],ii)
            isolatd.append(i)
        elif len(ii) == 2: 
     #       print("TAIL: ", i, skelCoords[i],ii)
            endBranches.append(i)
        elif len(ii) >= 4: 
            juncts.append(i)
        #else:
         #   juncts.append(i)
            #print("JUNC: ", i, skelCoords[i],ii)
    ends = np.array(endBranches)
    loners = np.array(isolatd)
    juncs = np.array(juncts)
    return (loners,juncs,ends)

def order_coords(skelCoords,origXY,termXY):

    #predictor-corrector search for nearest neighbor, non-identity.

    #Create a tree for query.
    skeletonTree = cKDTree(skelCoords) 

    #Get the origin and nearest neighbor indices.
    dOrig,indxOrig = skeletonTree.query(origXY,k=2)
    orderd=np.array(indxOrig)

    #index of the terminus.
    dTerm,indxTerm = skeletonTree.query(termXY,k=1)
    #print("Terminus: ",indxTerm,skelCoords[indxTerm])
    
    numAttmpts = 0
    numSuccess = 0
    failIncr = 0
    tossl = np.zeros(2, dtype=float)

    done = False
    while( not done):
         numAttmpts += 1
         v1 = skelCoords[orderd[-2]] 
         v2 = skelCoords[orderd[-1]] 
         predctVec = unitVec(v2-v1)
         v3 = v2+predctVec+tossl
         tossl[0] = 0.
         tossl[1] = 0.
         #print("attempts: ",numAttmpts,"\tSuccess: ",numSuccess)
         
         dOrig,indxOrig = skeletonTree.query(v3,k=2)

         if indxOrig[0] in orderd: # if it's in the list, jossle, moved on.
             failIncr += 1
             tossl += (failIncr*stepSze*arbStep())
             #print("FAILED ", indxOrig[0], "coord: ", skelCoords[indxOrig[0]])
         else: # otherwise add to list.
             failIncr = 0
             orderd = np.append(orderd, indxOrig[0])
             numSuccess += 1
             #print(numSuccess, " added indx ", indxOrig[0], "coord: ", skelCoords[indxOrig[0]])

         if (indxTerm in orderd) or (numAttmpts>bigNum):   
             done = True
             #if(numAttmpts>bigNum):
                 #print('\n')
                 #print('Iterated to death, could not trace curve!')
                 #print('Try running again - random seed is based on a time input!')
                 #print('Or use a larger step forward in the tracing algorithm (stepSze)...') 
                 #print('\n')
                 #print('...now exiting, bye...')
                 #print('\n')
                 #exit()
         #elif numAttmpts == 491:
         #    done = True

    numCoords = skelCoords.shape[0]
    print("Found ", numSuccess, "points of ", numCoords,  " total.", numSuccess/numCoords) 
    print("Algorithm efficiency (irrel.) ", numSuccess/numAttmpts, "iters: ",numAttmpts)
    

    #exit()
    #numCoords = skelCoords.shape[0]

    skel_XY = np.array(skelCoords[orderd])
    #print(skel_XY[:10])

    return skel_XY

def unitVec(v):
    #norm = np.linalg.norm(v)
    norm = np.sqrt((v**2).sum())
    if norm == 0: 
       return v
    return v/norm

def plotter1(image,skeleton,strngTitle):

    # display results
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4),sharex=True,sharey=True)

    #ax = axes.ravel()
    #ax = axes

    ax[0].imshow(image, cmap=plt.cm.gray)
    ax[0].axis('off')
    ax[0].set_title('original', fontsize=20)

    ax[1].imshow(skeleton, cmap=plt.cm.gray)
    #ax[1].imshow(np.invert(skeleton), cmap=plt.cm.gray)
    #ax[1].plot(sk3[:,1],sk3[:,0],'.',c='r',markersize=2.5)
    #ax[1].plot(endCoords[:,1],endCoords[:,0],'.',c='r',markersize=2.5)
    ax[1].axis('off')
    ax[1].set_title(strngTitle, fontsize=20)

    fig.tight_layout()
    plt.show()
    #plt.savefig("coilSkeleton.png")

def arbStep():

    theta = 2*math.pi*random.random()
    vec = np.array([math.cos(theta),math.sin(theta)],float)
    return vec

def plotter(image,endCoords,sk3,skeleton):

    # display results
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(8, 4),sharex=True,sharey=True)

    #ax = axes.ravel()
    #ax = axes

    ax[0].imshow(image, cmap=plt.cm.gray)
    ax[0].axis('off')
    ax[0].set_title('original', fontsize=20)

    coordLim = 2000
    ax[1].imshow(np.invert(skeleton), cmap=plt.cm.gray)
    ax[1].plot(sk3[:,1],sk3[:,0],'.',c='r',markersize=2.5)
    #ax[1].plot(endCoords[:,1],endCoords[:,0],'.',c='r',markersize=2.5)
    ax[1].axis('off')
    ax[1].set_title('skeleton, trimmed', fontsize=20)

    fig.tight_layout()
    #plt.savefig("coilSkeletonTrimmed.png")
    plt.show()


def doSciencePleaseThanks(skel,origXY):

    a=1.5
    b=0.22

    #get coordinates as a function of angle.
    ii = 0
    windingNum = 0
    incrLngth = 0.
    vecSav = (-1)*np.ones(2, dtype=int)
    v0 = np.copy(origXY)
    rads = []
    nmb = []
    thetas = []
    logspiral = []
    labCoords = []

    for coord in skel:
        v = coord - origXY
        dv = coord - v0
        incrLngth += np.sqrt((dv**2).sum())
        rad = np.sqrt((v**2).sum())
        theta = np.arctan2(v[0],v[1])
        rad = np.sqrt((v**2).sum())
        #print(v[1],-v[0])
        vLabCoord = np.array([v[1],-v[0]],float)
        #print("vec: ",v[1],v[0])
        if ( (v[0] < 0) and (vecSav[0] >= 0) ): #and (vecSav[1] >= 0) ):
            windingNum += 1
            #print("wound")
        theta += 2.*windingNum*np.pi
        #print(rad,theta)
        ii += 1
        rads.append(rad)
        thetas.append(theta)
        nmb.append(ii)
        vecSav = np.copy(v)
        v0 = np.copy(coord)
        #print("vecSav: ",vecSav[1],vecSav[0])
        logspiral.append(a*np.exp(b*theta))
        labCoords.append(vLabCoord)

      #  print(theta,rad)
        
    print('Estmated length of the coil, in pixels: ', incrLngth)

    strides = 11
    coords = np.asarray(labCoords)
    abbrvCoords = coords[1::strides]
    curvy = getCurvature(abbrvCoords)

    fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(12, 4))
    #fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(12, 4),sharey=True)

    ax[0].plot(nmb,rads,'.')
    ax[0].set_xlabel('coil pixel #, ordered from origin')
    ax[0].set_ylabel('radial distance from origin, in pixels', fontsize=12)
    ax[0].set_ylim([0,400])

    ax[1].plot(thetas,rads,'.',c='k',markersize=0.75)
    ax[1].xaxis.set_ticks(np.arange(0,10*np.pi,np.pi))

    #ax[1].xaxis.set_major_formatter(tck.FormatStrFormatter('2$\pi$'))
    #ax[1].xaxis.set_major_formatter(tck.FormatStrFormatter('%g'))
    #ax[1].xaxis.set_major_locator(plt.MultipleLocator(2 * np.pi))
    ax[1].xaxis.set_major_formatter(tck.FormatStrFormatter('%.4g'))
    ax[1].xaxis.set_major_locator(tck.MultipleLocator(base=2.0*np.pi))



    #labels=ax[1].get_xticks().tolist()
    #labels[1]='change'
    #ax[1].set_xticklabels(labels)
    #ax[1].plot(thetas,logspiral,'-',c='dodgerblue',label='logspiral curve')
    ax[1].set_xlabel('angle theta')
#plt.xticks((-1, 0, 1), ('$-1$', r'$\pm 0$', '$+1$'), color='k', size=20)

    #_=plt.xticks(rotation=75)

    ax[2].plot(thetas[1::strides],curvy,'-',c='r')
    ax[2].xaxis.set_major_formatter(tck.FormatStrFormatter('%.4g'))
    ax[2].xaxis.set_major_locator(tck.MultipleLocator(base=2.0*np.pi))
    ax[2].set_xlabel('angle theta')
    ax[2].set_ylabel('curvature', fontsize=12)


    fig.tight_layout()
    plt.savefig("r_vs_theta.png")
    #plt.show()


def getCurvature(xy):

    dx = np.gradient(xy[:, 0])
    dy = np.gradient(xy[:, 1])

    d2x = np.gradient(dx)
    d2y = np.gradient(dy)

    curvature = np.abs(d2x * dy - dx * d2y) / (dx * dx + dy * dy)**1.5
    #print(curvature)
    return(curvature)

if __name__ == '__main__':
    main()

