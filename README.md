#### Spring Project

This repository holds all the files that attempts to measure and plot the curvature of the mainsprings from a clock. This work mainly focuses on the image processing of the spring images to input into python.

#### Contributors

All contributors to this project can be found in the contributors.txt file.

#### Workflow

Convert the image to binary colors
Manually find the start and endpoint of the spring
Use skelTrimOrdr.py to skeletonize and perform calculatoins on the image

#### Image Processing

The code inside contour.py takes the image mainspring1.png and converts the image into a black and white image. This image is then saved as contour-spring.png and is more suitable to skeletonize. The act of skeletonizing turns an image into a sketch of the image one pixel thick.

To run this code and contour manspring1.png:

    $ python contour.py

The filter.py script converts the image to black and white. It currently converts the logspiral.png to a white spiral on a black background.the black and white image can then be inserted into skelTrimOrdr.py. To run the script:

    $ python filter.py

#### Clay Images

The focusing.py script masks the first clay image and makes it black and white. works for IMG-0412.jpg

Running the code is very simple for masking the first clay image

    $ python focusing.py

#### Skeletonizing

Use skelTrimOrdr.py to skeletonize. You must specify the picture you want to skeletonize as well as mark the start and end points of the spring. After the code had been modified, it takes the filtered image from filter.pylabeled as "filtered-logspiral.png"  and performs skeletonization, trims the skeleton, and then performs calculations on r vs. theta.

    $ python skelTrim0rdr.py "picture name"
