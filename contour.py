import numpy as np
import matplotlib.pyplot as plt
import cv2

def contrast(image):#image is a file name that you want to edit has to be entered as a string

    im = cv2.imread(image)
    #print(im)
    imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    plt.imshow(imgray,cmap=plt.cm.Greys)
    #plt.show()

    ret,thresh = cv2.threshold(imgray,127,255,0)
    #print(ret,thresh)
    plt.imshow(thresh,cmap=plt.cm.Greys)
    #plt.show()
    plt.savefig("pictures/contour-spring.png")

    #img = cv2.drawContours(im, contours, -1, (0,255,0), 3)

    #print(im)

    return im #returns a new image (img) that is black and white
#this works to make the 'worm'! dont change above!

contrast(image="pictures/mainspring1.png")



