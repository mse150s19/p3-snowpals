import skimage
import cv2
import numpy as np
from skimage import data
from skimage import color
from skimage import io
from PIL import Image
import matplotlib.pyplot as plt
from scipy.misc import imread, imsave


image = color.rgb2gray(io.imread('clay_imprints/IMG_0142.JPG'))
image = np.asanyarray(image)
print(image.mean())

nrows, ncols = image.shape
row, col = np.ogrid[:nrows, :ncols]
cnt_row, cnt_col = nrows / 2.25, ncols / 2.25
outer_disk_mask = ((row - cnt_row)**2 + (col - cnt_col)**2 > (nrows / 3.75)**2)
image[outer_disk_mask] = 1


image = image * 255 
cv2.imwrite('masked_image.jpg', image)

image = cv2.resize(image,None,fx=0.4,fy=0.4)
cv2.imshow("example",image)
cv2.waitKey(0)
cv2.destroyAllWindows()

work_img = imread('masked_image.jpg', mode = 'L')
threshold = 150
binarized = 1.0 * (work_img > threshold)
binarized = binarized * 255
cv2.imwrite('pictures/binarized.jpg', binarized)
binarized = cv2.resize(binarized,None,fx=0.4,fy=0.4)
cv2.imshow('Black_and_white', binarized)
cv2.waitKey(0)

